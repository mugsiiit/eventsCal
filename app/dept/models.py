from flask_sqlalchemy import SQLAlchemy
from app import db

class Dept(db.Model):
    __tablename__='events'
    dept_id=db.Column(db.Integer,db.ForeignKey('departments.id'))
    event_id=db.Column(db.Integer,primary_key=True,autoincrement=True)
    event_name=db.Column(db.String(50))
    event_venue=db.Column(db.String(100))
    event_time=db.Column(db.String(20))
    event_info=db.Column(db.String(100))
    event_date=db.Column(db.String(20))

    def __init__(self,dept_id,event_name,event_venue,event_time,event_date,event_info):
        self.dept_id=dept_id
        self.event_name=event_name
        self.event_venue=event_venue
        self.event_time=event_time
        self.event_date=event_date
        self.event_info=event_info

    def to_dict(self):
        return {
            'dept_id':self.dept_id,
            'event_id':self.event_id,
            'event_name':self.event_name,
            'event_venue':self.event_venue,
            'event_time':self.event_time,
            'event_date':self.event_date,
            'event_info':self.event_info,
        }
    def __repr__(self):
        return "Dept %s <%d>" %(self.event_name,self.dept_id)
