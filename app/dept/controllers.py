from flask import Blueprint, request, session, jsonify
from sqlalchemy.exc import IntegrityError
from app import db,requires_auth
from .models import Dept
from app.user.models import User
from app.admin.models import Admin
from app.users.models import Users
from app.event.models import Event

mod_dept=Blueprint('dept',__name__,url_prefix='/api')

@mod_dept.route('/dept',methods=['POST'])
@requires_auth
def create_event():
    user_id=session['user_id']
    dept = Admin.query.filter(Admin.head_id == user_id).first()
    if dept is not None:
        event_name=request.form['event_name']
        event_venue=request.form['event_venue']
        event_time=request.form['event_time']
        event_date=request.form['event_date']
        event_info=request.form['event_info']
        print(user_id)
        dept_id = dept.id
        event=Dept.query.filter(Dept.event_name==event_name,Dept.dept_id==dept.id).first()
        if event is None:
            event=Dept(dept_id,event_name,event_venue,event_time,event_date,event_info)
            db.session.add(event)
            db.session.commit()
            event=Dept.query.filter(Dept.event_name==event_name).first()
            users3=Users.query.filter(Users.dept_id==dept.id).all()
            for user2 in users3:
                event2=Event(user2.user_id,event.event_id,'something')
                db.session.add(event2)
                db.session.commit()
            return jsonify(success=True,event=event.to_dict())
        return jsonify(success=False,message="This event already exists")
    return jsonify(success=False)
@mod_dept.route('/dept',methods=['GET'])
@requires_auth
def get_all_events():
    user_id = session['user_id']
    user=User.query.filter(User.id==user_id).first()
    #print(users)
    print("entered")
    if user is not None:
        print("entered2")
        dept=Admin.query.filter(Admin.head_id==user.id).first()
        if dept is not None:
        
            events=Dept.query.filter(Dept.dept_id==dept.id).all()
            if events is not None:
                return jsonify(success=True , events=[event.to_dict() for event in events])
            return jsonify(success=False)
        return jsonify(success=False)
    return jsonify(success=False)

@mod_dept.route('/dept/user',methods=['GET'])
@requires_auth
def get_all_users():
    user_id=session['user_id']
    dept=Admin.query.filter(Admin.head_id==user_id).first()
    if dept is not None:
        users=Users.query.filter(Users.dept_id==dept.id).all()
        ans2=[]
        for user in users:
            ans=user.to_dict()
            user2=User.query.filter(User.id == user.user_id).first()
            ans['dept_name']=dept.dept_name
            ans['email_id']=user2.email
            ans2.append(ans) 
        return jsonify(success=True , users=ans2)
    return jsonify(success=False,message="hkvk")

@mod_dept.route('/dept/<id>',methods=['GET'])
@requires_auth
def get_event(id):
    user_id=session['user_id']
    user=User.query.filter(User.id==user_id).first()
    dept=Admin.query.filter(Admin.head_id==user.id).first()
    event=Dept.query.filter(Dept.dept_id==dept.id, Dept.event_id==id).first()
    if event is None:
        return jsonify(success=False), 404
    return jsonify(success=True,event=event.to_dict())

@mod_dept.route('/dept/user/<id>' , methods=['GET'])
@requires_auth
def get_user(id):
    user_id=session['user_id']
    dept=Admin.query.filter(Admin.head_id==user_id).first()
    user=Users.query.filter(Users.user_id==id , Users.dept_id==dept.id).first()
    if user is None:
        return jsonify(success=False), 404
    return jsonify(success=True,user=user.to_dict())

@mod_dept.route('/dept/edit' , methods=['POST'])
@requires_auth
def edit_event():
    user_id=session['user_id']
    name=request.form['event_name']
    event2=Dept.query.filter(Dept.event_name==name).first()
    id=event2.event_id
    user=User.query.filter(User.id==user_id).first()
    dept=Admin.query.filter(Admin.head_id==user.id).first()
    if dept is not None:
        event=Dept.query.filter(Dept.dept_id==dept.id, Dept.event_id==id).first()
        if event is None:
            return jsonify(success=False), 404
        event.event_time=request.form['event_time']
        event.event_venue=request.form['event_venue']
        event.event_info=request.form['event_info']
        event.event_date=request.form['event_date']
        db.session.commit()
        return jsonify(success=True)
    return jsonify(success=False)

@mod_dept.route('/dept/<id>/delete',methods=['POST'])
@requires_auth
def delete_event(id):
    user_id=session['user_id']
    user=User.query.filter(User.id==user_id).first()
    dept=Admin.query.filter(Admin.head_id==user.id).first()
    if dept is not None:
        event=Dept.query.filter(Dept.dept_id==dept.id, Dept.event_id==id).first()
        if event is None:
            return jsonify(success=False), 404
        db.session.delete(event)
        db.session.commit()
        return jsonify(success=True)
    return jsonify(success=False)
@mod_dept.route('/dept/user/<id>/delete' , methods=['POST'])
@requires_auth
def delete_user(id):
    user_id=session['user_id']
    dept=Admin.query.filter(Admin.head_id==user_id).first()
    if dept is not None and  dept.head_id!=user_id:
        user=Users.query.filter(Users.user_id==id , Users.dept_id==dept.id).first()
        if user is None:
            return jsonify(success=False),404
        db.session.delete(user)
        db.session.commit()
        return jsonify(success=True) 

    return jsonify(success=False)

@mod_dept.route('/dept/user',methods=['POST'])
@requires_auth
def create_user():
    email_id=request.form['email_id'] 
    id=session['user_id']
    user=User.query.filter(User.id==id).first()
    user4=User.query.filter(User.email==email_id).first()
    dept=Admin.query.filter(Admin.head_id == user.id).first()
    if dept is not None:
        user2=Users.query.filter(Users.user_id==user4.id , Users.dept_id==dept.id).first()
        user3=User.query.filter(User.email==email_id).first()
        if user2 is not None or user3 is None:
            return jsonify(success=False,message="User already exists")   
        u=Users(dept.id,user4.id)
        db.session.add(u)
        db.session.commit()
        events=Dept.query.filter(Dept.dept_id==dept.id).all()
        for e in events:
            e2=Event(user4.id,e.event_id,"something")
            db.session.add(e2)
            db.session.commit()
        return jsonify(success=True,u=u.to_dict())
    return jsonify(success=False)

@mod_dept.route('/dept/calendar',methods=['GET'])
@requires_auth
def get_all_events2():
    user_id=session['user_id']
    dept=Admin.query.filter(Admin.head_id==user_id).first()
    if dept is not None:
        events=Dept.query.all()
        if events is not None:
            return jsonify(success=True,events=[event.to_dict() for event in events])
        return jsonify(success=False)
    return jsonify(success=False)


