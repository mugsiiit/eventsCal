var usersManager = (function() {
    var norsvp = function() {
	id=$(this).data('id');
	console.log(id);
	$.ajax({
	    method : 'post',
	    url:'/api/users/' + id + '/norsvp',
	    success : function(res) {
                if(res.success)
                {console.log("rejected");
                alert('You are going to miss lots of fun')
                
		listAllEvents();}
                else page('/login');
	    }
	})
    }
    var yesrsvp = function() {
	id=$(this).data('id');
	console.log(id);
	$.ajax({
	    method: 'post',
	    url : '/api/users/' + id +'/yesrsvp',
	    success: function(res) {
               if(res.success)
               { console.log("confirmed");
                alert('Yeahhh!!! You are coming for the event')
		listAllEvents();}
               else
                  page('/login');
	    }
	})
    }
    var listAllEvents = function() {
        $.ajax({
            method:'get',
            url:'/api/users',
            success : function(res) {
                if(res.events){
                console.log(res.events);
                viewManager.render('users-list',{
                    events : res.events,events2: res.events2,events3 : res.events3,
                }, function($view) {
		    $view.find(".no").click(norsvp);
		    $view.find(".yes").click(yesrsvp);
		}); }
                else page('/login');
            }
        });
    }
    var listCalendar = function() {
        $.ajax({
	    method:'get',
	    url:'/api/users/calendar',
	    success: function(res) {
		console.log(res.events);
		viewManager.render('users-list-calendar',{
		    events : res.events,
		});
	    }
	});
    }
		
			

    var listOne = function (id) {
        $.ajax({
            method: 'get',
            url: '/api/users',
            success: function(res) {
                console.log(res.events[0]);
                viewManager.render('users', res.events[0]);
            }
        })
    };
var tManager={};
tManager.listAllEvents=listAllEvents;
    tManager.listOne=listOne;
    tManager.listCalendar=listCalendar;
return tManager;

})();     
