var deptManager = (function () {
    var editTask = function () {
        name=$(this).data('id');
        viewManager.render('dept2', {
            formAction: '/api/dept/edit',
            name : name,
        }, function ($view) {
            console.log($view);
            $view.submitViaAjax(function ( response) {
                page('/dept');
            });
        });
    };
    
    var deleteTask = function() {
        id = $(this).data('id');
        console.log(id);
        $.ajax({
            method: 'post',
            url: '/api/dept/' + id + '/delete',
            success: function(res) {
               if(res.success)
                listAll();
               else {page('/logout')}
            }
        })
    }
   
    var deleteTaskUser = function() {
        id = $(this).data('id');
        console.log(id);
        $.ajax({
            method: 'post',
            url: '/api/dept/user/' + id + '/delete',
            success: function(res) {
               if(res.success)
                listAllUsers();
               else {page('/logout')}
            }
        })
    }

    var listAll = function () {
        $.ajax({
            method: 'get',
            url: '/api/dept',
            success: function(res) {
                if(res.events!==undefined){
                console.log("sravya");
                viewManager.render('depts-list', {
                    events: res.events,
                }, function($view) {
                    $view.find(".edit-task").click(editTask);
                    $view.find(".delete-task").click(deleteTask);
                }); }
               else {console.log("soumya");page('/logout');}
            }
        });
    };
    var listAllUsers = function() {
       $.ajax({
            method: 'get',
            url: '/api/dept/user',
            success: function(res) {
                if(res.users!==undefined){
                console.log(res.users);
                viewManager.render('depts2-list', {
                    users: res.users,
                }, function($view) {
                    $view.find(".delete-task").click(deleteTaskUser);
                }); }
else {page('/logout');}
            }
        });
    };

    var create = function () {
        viewManager.render('dept', {
            formAction: '/api/dept',
        }, function ($view) {
            console.log($view);
            $view.submitViaAjax(function ( response) {
              if(response.success)
              {  page('/dept');console.log("success");}
              else
              {  page('/logout');console.log("failure");}
            });
        });
    };
    
    var create2 = function () {
       viewManager.render('dept3', {
            formAction: '/api/dept/user',
        }, function ($view) {
            console.log($view);
            $view.submitViaAjax(function ( response) {
                page('/dept/user');
            });
        });
    };


    var listOne = function (id) {
        $.ajax({
            method: 'get',
            url: '/api/dept',
            success: function(res) {
                console.log(res.events[0]);
                viewManager.render('dept', res.events[0]);
            }
        })
    };
    var listOneUser = function(id) {
	$.ajax({
            method: 'get',
            url: '/api/dept/user',
            success: function(res) {
                console.log(res.users[0]);
                viewManager.render('dept3', res.users[0]);
            }
        })
    };

    var tManager = {};
    tManager.listAll = listAll;
    tManager.listOne = listOne;
    tManager.listOneUser = listOneUser
    tManager.create = create;
    tManager.listAllUsers=listAllUsers;
    tManager.create2 = create2;
    return tManager;
})();
