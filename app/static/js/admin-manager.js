var adminManager = (function () {

    var deleteTask = function() {
        id = $(this).data('id');
        $.ajax({
            method: 'post',
            url: '/api/admin/' + id + '/delete',
            success: function(res) {
                listAll();
            }
        })
    }
    var listAll = function () {
        $.ajax({
            method: 'get',
            url: '/api/admin',
            success: function(res) {
                console.log(res.admins);
                if(res.admins!==undefined){
		viewManager.render('admins-list', {
                    admins: res.admins,
                }, function($view) {
                    $view.find(".delete-task").click(deleteTask);
                });
                  }
                else {page('/logout');}
            }
        });
    };
    var listUsers = function() {
	$.ajax({
	    method:'get',
	    url:'/api/admin/users',
	    success: function(res) {
		console.log(res.users);
		if(res.users!==undefined){
		    viewManager.render('admin-users-list', {
			users:res.users,
		    });
		}
		else{
		    page('/logout');
		}
	    }
	})
    }
	    
    var create = function () {
        viewManager.render('admin', {
            formAction: '/api/admin',
        }, function ($view) {
            console.log($view);
            $view.submitViaAjax(function (response) {
                page('/admin');
            });
        });
    };

    var listOne = function (id) {
        $.ajax({
            method: 'get',
            url: '/api/admin',
            success: function(res) {
                console.log(res.admins[0]);
                viewManager.render('admin', res.admins[0]);
            }
        })
    };

    var tManager = {};
    tManager.listAll = listAll;
    tManager.listOne = listOne;
    tManager.create = create;
    tManager.listUsers = listUsers;
    return tManager;
})();
