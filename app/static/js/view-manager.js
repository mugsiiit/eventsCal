var viewManager = (function () {
    var vm = {};
    var $root = $('#content');

    var renderTemplate = function (template, data, callback) {
        var compiledTemplate = Handlebars.compile(template);
        var renderedHtml = compiledTemplate(data);
        var $node = $(renderedHtml);
        $root.html( $node );
        if ($.isFunction(callback)) {
            callback($node);
        }
    };

    var render = function(view, data, callback) {

        if ($.isFunction(data)) {
            callback = data;
            data = {};
        }
        $.get({
            url:'/static/templates/' + view + '.html',
            success: function (response) {
                renderTemplate(response, data, callback);
            }
        });
    };

    vm.render = render;
    return vm;
})();
