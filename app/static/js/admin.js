var makeAdmin = function (todoData) {
    var data = {
        dept_name: todoData.dept_name,
        head_id: todoData.head_id,
    };
    var getData = function () {
        return data;
    };
    var t = {};
    t.getData = getData;
    return t;
};
