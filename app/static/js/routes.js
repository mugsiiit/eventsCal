
page('/admin', authManager.requiresAuthentication, adminManager.listAll);
page('/admin/create', authManager.requiresAuthentication, adminManager.create);
page('/admin/users' , authManager.requiresAuthentication,adminManager.listUsers);
page('/dept',authManager.requiresAuthentication , deptManager.listAll);
page('/dept/create', authManager.requiresAuthentication, deptManager.create);
page('/dept/user',authManager.requiresAuthentication, deptManager.listAllUsers);
page('/dept/create2',authManager.requiresAuthentication, deptManager.create2)
page('/users',authManager.requiresAuthentication , usersManager.listAllEvents);
page('/calendar',authManager.requiresAuthentication , usersManager.listCalendar);

page('/login', authManager.showLogin);
page('/logout', authManager.logout);
page('/register', authManager.showRegister);
page('/',authManager.showRegister);
page({});
