var makeDept = function (todoData) {
    var data = {
        event_id: todoData.event_id,
        event_name: todoData.event_name,
        event_venue: todoData.event_venue,
        event_time: todoData.event_time,
        event_info: todoData.event_info,
    };
    var getData = function () {
        return data;
    };
    var t = {};
    t.getData = getData;
    return t;
};
