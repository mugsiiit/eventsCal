from flask_sqlalchemy import SQLAlchemy
from app import db

class Admin(db.Model):
    __tablename__ = 'departments'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    head_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    dept_name=db.Column(db.String(200))

    def __init__(self, head_id,dept_name):
        self.head_id=head_id
        self.dept_name=dept_name

    def to_dict(self):
        return {
            'id':self.id,
            'head_id':self.head_id,
            'dept_name':self.dept_name,
        }

    def __repr__(self):
        return "Admin<%d> %s" % (self.id, self.dept_name)
