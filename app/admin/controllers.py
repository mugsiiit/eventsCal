from flask import Blueprint, request, session, jsonify
from app import db, requires_auth
from .models import Admin
from app.user.models import User
from app.dept.models import Dept
from app.users.models import Users

mod_admin = Blueprint('admin', __name__, url_prefix='/api')

@mod_admin.route('/admin', methods=['POST'])
@requires_auth
def create_admin():
    user_id=session['user_id']
    if user_id==1:
        email_id = request.form['email_id']
        dept_name = request.form['dept_name']
        admin=Admin.query.filter(Admin.dept_name==dept_name).first()
        users=User.query.filter(User.email==email_id ).first()
        users2=User.query.all()
        print(users)
        if users is None:
            return jsonify(success=False,message="something went wrong")
        if admin is None:
            users.type="dept"
            db.session.commit()
            admin = Admin(users.id,dept_name)
            db.session.add(admin)
            db.session.commit()
            dept=Admin.query.filter(Admin.dept_name==dept_name).first()
            user=Users(dept.id,users.id)
            db.session.add(user)
            db.session.commit()
            ans=admin.to_dict()
            ans['email_id']=email_id
            return jsonify(success=True, admin=ans)
        return jsonify(success=False , message="This head is already present")
    return jsonify(success=False) , 404

@mod_admin.route('/admin', methods=['GET'])
@requires_auth
def get_all_admins():
    user_id = session['user_id']
    user=User.query.filter(User.id==user_id).first()
    if user.id==1:
        admins = Admin.query.all()
        print(admins)
        ans2=[]
        for admin in admins:
            ans=admin.to_dict()
            user=User.query.filter(User.id==admin.head_id).first()
            ans['email_id']=user.email
            ans2.append(ans)
        return jsonify(success=True, admins=ans2)
    return jsonify(success=False,message="khfkv")

@mod_admin.route('/admin/<id>', methods=['GET'])
@requires_auth
def get_admin(id):
    user_id = session['user_id']
    admin = Admin.query.filter(Admin.id == id).first()
    if admin is None:
        return jsonify(success=False), 404
    else:
        return jsonify(success=True, admin=admin.to_dict())

@mod_admin.route('/admin/<id>', methods=['POST'])
@requires_auth
def edit_admin(id):
    user_id = session['user_id']
    if user_id==1:
        admin = Admin.query.filter(Admin.id == id).first()
        if admin is None:
            return jsonify(success=False), 404
        else:
            admin.dept_name = request.form['dept_name']
            admin.head_id = request.form['head_id']
            db.session.commit()
            return jsonify(success=True)
    return jsonify(success=False)

@mod_admin.route('/admin/<id>/delete', methods=['POST'])
@requires_auth
def delete_admin(id):
    user_id = session['user_id']
    if user_id==1:
        admin = Admin.query.filter(Admin.id == id).first()
        if admin is None:
            return jsonify(success=False), 404
        else:
            db.session.delete(admin)
            db.session.commit()
            return jsonify(success=True)
    return jsonify(success=False) 

@mod_admin.route('/admin/users',methods=['GET'])
@requires_auth
def get_all_users():
    user_id=session['user_id']
    if user_id==1:
        users=User.query.filter(User.id!=1).all()
        return jsonify(success=True,users=[user.to_dict() for user in users])
    return jsonify(success=False) , 404


