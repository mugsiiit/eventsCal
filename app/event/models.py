from flask_sqlalchemy import SQLAlchemy
from app import db

class Event(db.Model):
    __tablename__='rsvp_info'
    sr_no=db.Column(db.Integer,primary_key=True,autoincrement=True)
    user_id=db.Column(db.Integer,db.ForeignKey('users.id'))
    event_id=db.Column(db.Integer,db.ForeignKey('events.event_id'))
    rsvp=db.Column(db.String(50))
    
    def __init__(self,user_id,event_id,rsvp):
        self.user_id=user_id
        self.event_id=event_id
        self.rsvp=rsvp

    def to_dict(self):
        return {
            'user_id':self.user_id,
            'event_id':self.event_id,
            'rsvp':self.rsvp,
        }
    def __repr__(self):
        return "Event<%d> <%d> %s" %(self.user_id,self.event_id,self.rsvp)
    
                       

