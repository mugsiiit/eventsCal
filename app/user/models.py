from flask_sqlalchemy import SQLAlchemy
from app import db
from werkzeug.security import generate_password_hash, check_password_hash

class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255),nullable=False)
    email = db.Column(db.String(255),unique=True,nullable=False)
    password = db.Column(db.String(255),nullable=False)
    mobile_no = db.Column(db.Integer,unique=True,nullable=False)
    type = db.Column(db.String(10))

    def __init__(self, name, email, password, mobile_no, type):
        self.name = name
        self.email = email
        self.password = generate_password_hash(password)
        self.mobile_no=mobile_no
        self.type=type
        
    def check_password(self, password):
        return check_password_hash(self.password, password)
    
    def to_dict(self):
        return {
            'id' : self.id,
            'name': self.name,
            'email': self.email,
            'mobile_no':self.mobile_no,
            'type':self.type,
        }

    def __repr__(self):
        return "User<%d> %s %s" % (self.id, self.name,self.type)

