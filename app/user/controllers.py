from flask import Blueprint, request, session, jsonify
from sqlalchemy.exc import IntegrityError
from app import db
from .models import User
import re
mod_user = Blueprint('user', __name__, url_prefix='/api')

@mod_user.route('/login', methods=['GET'])
def check_login():
    if 'user_id' in session:
        user = User.query.filter(User.id == session['user_id']).first()
        return jsonify(success=True, user=user.to_dict())

    return jsonify(success=False), 401


@mod_user.route('/login', methods=['POST'])
def login():
    try:
        email = request.form['email']
        password = request.form['password']
        type= request.form['type']
    except KeyError as e:
        return jsonify(success=False, message="%s not sent in the request" % e.args), 400

    user = User.query.filter(User.email == email).first()
    if user is None or not user.check_password(password):
        return jsonify(success=False, message="Invalid Credentials"), 400
    if user.type == 'users' and type=='admin':
        return jsonify(success=False, message="Invalid Type"), 400
    if user.type=='users' and type=='dept':
        return jsonify(success=False, message="Invalid type"), 400
    if user.type=='dept' and type=='admin':
        return jsonify(success=False, message="Invalid type"), 400
    session['user_id'] = user.id
    session['type'] = user.type;
    user=user.to_dict()
    user['form_type']=type
    return jsonify(success=True, user=user)

@mod_user.route('/logout', methods=['POST'])
def logout():
    session.pop('user_id')
    session.pop('type')
    return jsonify(success=True)

@mod_user.route('/register', methods=['POST'])
def create_user():
    try:
        name = request.form['name']
        email = request.form['email']
        password = request.form['password']
        mobile_no=request.form['mobile_no']
        #type=request.form['type']
    except KeyError as e:
        return jsonify(success=False, message="%s not sent in the request" % e.args), 400

    if '@' not in email:
        return jsonify(success=False, message="Please enter a valid email"), 400
    prog=re.compile(r"^[789]\d{9}$")
    result=prog.match(mobile_no)
    if not result:
        return jsonify(success=False, message="Please enter a valid mobile"), 400
    users=User.query.all()
    print(users)
    if name=="sravya":
        type="admin"
    else:
        type="users"
    u = User(name, email, password, mobile_no, type)
    db.session.add(u)
    try:
        db.session.commit()
    except IntegrityError as e:
        return jsonify(success=False, message="This email already exists"), 400

    return jsonify(success=True)
