from flask import Blueprint, request, session, jsonify
from app import db, requires_auth
from .models import Users
from app.dept.models import Dept
from app.event.models import Event
from app.user.models import User
mod_users=Blueprint('users',__name__,url_prefix='/api')

@mod_users.route('/users',methods=['GET'])
@requires_auth
def get_all_events():
    users=Users.query.filter(Users.user_id==session['user_id']).all()
    user=User.query.filter(User.id==session['user_id']).first()
    if user.type=='users' or user.type=="dept" or user.type=='admin':
        events=Dept.query.all()
        events=[]
        eventss=[]
        eventss2=[]
        for user in users:
            q=[]
            q2=[]
            q3=[]
            events2=Dept.query.filter(Dept.dept_id==user.dept_id).all()
            print(events2)
            for event in events2:
                event4=event.to_dict()
                event6=Event.query.filter(Event.user_id==session['user_id'],Event.event_id==event.event_id).first()
                event4['rsvp']=event6.rsvp
                event4['user_id']=session['user_id']
                if event6.rsvp=='something':
                    q.append(event4)
                elif event6.rsvp=='true':
                    q2.append(event4)
                else:
                    q3.append(event4)
            events.extend(q)
            eventss.extend(q2)
            eventss2.extend(q3)
        return jsonify(success=True,events=events,events2=eventss,events3=eventss2)
    return jsonify(success=False)


@mod_users.route('/users/<id>' , methods=['GET'])
@requires_auth
def get_event_id(id):
    user_id=session['user_id']
    event=Event.query.filter(Event.user_id==user_id,Event.event_id==id).first()
    if event is not None:
        return jsonify(success=True,event=event.to_dict())
    return jsonify(success=False), 404

@mod_users.route('/users/<id>/yesrsvp',methods=['POST'])
@requires_auth
def rsvp_yes(id):
    user_id=session['user_id']
    events2=Event.query.all()
    user=User.query.filter(User.id==user_id).first()
    if user.type=='users' or user.type=='admin' or user.type=='dept':
        event=Event.query.filter(Event.user_id==user_id,Event.event_id==id).first()
        if event is None:
            return jsonify(success=False), 404
        event.rsvp='true'
        db.session.commit()
        return jsonify(success=True)
    return jsonify(success=False)

@mod_users.route('/users/<id>/norsvp',methods=['POST'])
@requires_auth
def rsvp_no(id):
    user_id=session['user_id']
    user=User.query.filter(User.id==user_id).first()
    if user.type=='users' or user.type=='admin' or user.type=='dept':
        event=Event.query.filter(Event.user_id==user_id,Event.event_id==id).first()
        if event is None:
            return jsonify(success=False) , 404
        event.rsvp='false'
        db.session.commit()
        return jsonify(success=True)
    return jsonify(success=False)

@mod_users.route('/users/calendar',methods=['GET'])
@requires_auth
def get_calendar():
    user_id=session['user_id']
    events=Dept.query.all()
    if events is not None:
        return jsonify(success=True,events=[event.to_dict() for event in events])
    return jsonify(success=False)
