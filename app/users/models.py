from flask_sqlalchemy import SQLAlchemy
from app import db

class Users(db.Model):
    __tablename__='dept_subscription'
    subs_id=db.Column(db.Integer,primary_key=True,autoincrement=True)
    dept_id=db.Column(db.Integer,db.ForeignKey('departments.id'))
    user_id=db.Column(db.Integer,db.ForeignKey('users.id'))

    def __init__(self,dept_id,user_id):
        self.dept_id=dept_id
        self.user_id=user_id

    def to_dict(self):
        return {
            'subs_id':self.subs_id,
            'dept_id':self.dept_id,
            'user_id':self.user_id,
        }
    def __repr__(self):
        return "Users<%d> %d" % (self.dept_id , self.user_id)
